#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#define UPDATE_TIMEOUT 30000
#define SSID "Ne moy wifi i tochka"
#define PASSWORD "Ma18101976"
#define LED_BUILTIN 2

ESP8266WebServer server(80);
bool lastState = false;
unsigned long lastTime = 0;

void turnOn() {
  Serial.println("on");
  digitalWrite(LED_BUILTIN, LOW);
  lastState = true;
  lastTime = millis();
}

void turnOff() {
  Serial.println("off");
  digitalWrite(LED_BUILTIN, HIGH);
  lastState = false;
  lastTime = millis();
}

void handleOn() {
  turnOn();
  server.send(200, "text/plain", "on");
}

void handleOff() {
  turnOff();
  server.send(200, "text/plain", "off");
}

void setup(void) {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(115200);
  WiFi.softAP(SSID, PASSWORD);
 
  Serial.print("\nAP IP address: ");
  Serial.println(WiFi.softAPIP());

  server.on("/on", handleOn);
  server.on("/off", handleOff);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();

  static unsigned long lastTime = 0;

  if ((millis() - lastTime) > UPDATE_TIMEOUT)
  {
    if (lastState)
    {
      turnOff();
    }

    lastTime = millis();
  }
}